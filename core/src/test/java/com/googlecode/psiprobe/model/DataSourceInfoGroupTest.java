package com.googlecode.psiprobe.model;

import org.junit.Ignore;
import org.junit.Test;

import com.codebox.bean.JavaBeanTester;

/**
 * The Class DataSourceInfoGroupTest.
 */
public class DataSourceInfoGroupTest {

  /**
   * Javabean tester.
   */
  @Ignore
  @Test
  public void javabeanTester() {
    JavaBeanTester.builder(DataSourceInfoGroup.class).loadData().test();
  }

}
